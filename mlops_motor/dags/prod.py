# Databricks notebook source
from pyspark.sql.functions import col, array, when, array_remove, lit, abs
import pandas as pd
import numpy as np
import logging
import pickle
from datetime import date
import sys

# COMMAND ----------

# MAGIC %md # Here we should, in theory, bring back the necessary Repo functions

# COMMAND ----------

# MAGIC %run "./STG"

# COMMAND ----------

# MAGIC %run "./METADATA"

# COMMAND ----------

# MAGIC %run ./Logging

# COMMAND ----------

if METADATA['component']:
    log = LogDB('[PRO]','/tmp/'+METADATA['component'][0])
else:
    
    raise Exception("mandatory_key name_modelo not found")

# COMMAND ----------

meta = pd.DataFrame.from_dict(METADATA)

# COMMAND ----------

meta.head()

# COMMAND ----------

# MAGIC %md # Path to row data & binary-model 

# COMMAND ----------

path_to_model = meta["path_to_model"][0].split(",")
path_to_model = ", ".join(path_to_model)
path_to_data = meta["path_to_data_excecute"][0].split(",")
path_to_data = ", ".join(path_to_data)
path_to_data_train = meta["path_to_data_train"][0].split(",")
path_to_data_train = ", ".join(path_to_data_train)

# COMMAND ----------

# MAGIC %md # Data Drift

# COMMAND ----------

def datadrift(metadados):
  
  """
  Calculus of the data drift and data outlier for numeric features
  Input: Model´s Metadados 
  Output: 
  """ 
  #loggs = open("/dbfs/FileStore/shared_uploads/daniel.vargas@bv.com.br/teste.txt","a")
  
  tolerance = float(meta["porcentage_of_tolerance_df_metrics"][0])

  numeric_features = meta["numeric-features"][0].split(",")

  df_pre_std = data_prep(path_to_data_train)\
  .select(numeric_features)\
  .sample(withReplacement=True, fraction=1.0, seed=134)\
  .limit(350)\
  .summary()

  df_pos_std = data_prep(path_to_data)\
  .select(numeric_features)\
  .sample(withReplacement=True, fraction=1.0, seed=143)\
  .limit(350)\
  .summary()

  # get conditions for all columns except summary
  conditions_ = [when(abs(abs(df_pre_std[c]) - abs(df_pos_std[c])) > tolerance*abs(df_pre_std[c]) , lit(c)).otherwise("") for c in df_pre_std.columns if c!="summary"]

  select_expr =[
                  col("summary"), 
                  *[df_pos_std[c] for c in df_pos_std.columns if c!="summary"], 
                  array_remove(array(*conditions_), "").alias("drift_columns")
  ]

  df_drift = df_pre_std\
  .join(df_pos_std, "summary")\
  .select(*select_expr)\
  .select('summary', 'drift_columns')

  for item in range(df_drift.count()):
    drift = df_drift.collect()[item][1]
    if len(drift) != 0:
      if df_drift.collect()[item][0] == 'max' or df_drift.collect()[item][0] == 'min':
        #print("logging feature drift on", df_drift.collect()[item][0], drift)
        log.warn(F"logging features that drift above or below threshold on outliers {drift}") 
#         with open("/dbfs/FileStore/shared_uploads/daniel.vargas@bv.com.br/teste2.txt", "a") as loggs:
#           loggs.write("teste")
#           loggs.close()
      if df_drift.collect()[item][0] == 'mean':
        log.warn(F"logging features that drift above or below threshold on mean {drift}")
      if df_drift.collect()[item][0] == '50%':
        log.warn(F"logging features that drift above or below threshold on median {drift}")
      if df_drift.collect()[item][0] == '25%':
        log.warn(F"logging features that drift above or below threshold on 25 quartil {drift}")
      if df_drift.collect()[item][0] == '75%':
        log.warn(F"logging features that drift above or below threshold on 75 quartil {drift}")

# COMMAND ----------

log.start("[Datadrift]")
datadrift(meta)
log.stop("[Datadrift]")

# COMMAND ----------

# MAGIC %md # Apply model to row data

# COMMAND ----------

df = apply_model(path_to_model, path_to_data)

# COMMAND ----------

df.show()

# COMMAND ----------

def cria_BIG():
  """
  cria a BIG from df
  """
  return print("criei BIG")

cria_BIG()

# COMMAND ----------

log.printall()

# COMMAND ----------

destino = "dbfs:/FileStore/Eng/Models/"+METADATA['component'][0]+"/model.log"
dbutils.fs.mv("file:" + log.get_filename(), destino)