# Databricks notebook source
#!pip install pytest

# COMMAND ----------

#import pytest
import re
import unittest
from pandas.util.testing import assert_frame_equal
import warnings
import pandas as pd
import os.path
from os import path
from pathlib import Path
import pathlib

warnings.filterwarnings("ignore")

# COMMAND ----------

# MAGIC %run "./STG"

# COMMAND ----------

# MAGIC %run "./METADATA"

# COMMAND ----------

'''with open('/dbfs/FileStore/shared_uploads/daniel.vargas@bv.com.br/dic.pkl', 'rb') as f: 
    loaded_dict = pickle.load(f)
meta = pd.DataFrame.from_dict(loaded_dict)'''

# COMMAND ----------

# MAGIC %run ./Logging

# COMMAND ----------

if METADATA['name_modelo']:
    log = LogDB('[TU]','/tmp/'+METADATA['name_modelo'][0])
else:
    raise Exception("mandatory_key name_modelo not found")

# COMMAND ----------

# MAGIC %md # Metadata Validation

# COMMAND ----------

def valida_meta(metadata):
  
  keys_obrigatorias  = ['path_to_data_train','table_use_1','table_use_2','name_modelo','score_training_acc','AUC','path_to_model','component',
         'path_to_data_excecute','LAB','author_model','on_success','on_failure','libs','dataprep','test_dataprep',
         'test_load_model','apply_model','test_train','threshold-jen-war','threshold-ks-p-value','porcentage_of_tolerance_df_metrics','numeric-features']
  
  for i in metadata:
    if i not in keys_obrigatorias:
      log.assert_failed(F"A chave '{i}' não é uma chave válida")
    if (type(metadata[i]) != list):
      log.assert_failed(F"O valor da chave '{i}' não é uma lista")
    elif (len(metadata[i]) != 1):
      pos = len(metadata[i])
      log.assert_failed(F"A chave '{i}' deveria ter 1 posição, foi passado {pos} posições")
      
  for i in keys_obrigatorias:
    if i not in metadata:
      log.assert_failed(F"A chave '{i}' é obrigatória e não está presente nos metadados")
  
  log.info("Metadata Validation: Passed ")

valida_meta(METADATA)

# COMMAND ----------

meta = pd.DataFrame.from_dict(METADATA)

# COMMAND ----------

meta.head()

# COMMAND ----------

# MAGIC %md # Test path to training data

# COMMAND ----------

#Nova Versão - Checar com daniel se o fluxo está correto
test_dataprep_list = meta["path_to_data_train"][0].split(",")

def file_exists(path):
  try:
    dbutils.fs.ls(path)
    return True
  except Exception as e:
    if 'java.io.FileNotFoundException' in str(e):
      return False
    else:
      raise

for file in test_dataprep_list:
  if not file_exists(file):
      log.assert_failed(F"File {file} does not exists")
  #assert existe == "done"

log.info("Test Path to Training Data: Passed")

# COMMAND ----------

#Versão Anterior - Daniel
'''test_dataprep_list = meta["path_to_data_train"][0].split(",")
est_dataprep_list = ", ".join(test_dataprep_list)
#test_dataprep_list = "/dbfs/FileStore/tables/diabetes.csv"

if __name__ == "__main__":
  file = pathlib.Path(test_dataprep_list)
  if file.exists():
      existe = 'done'
  else:
      existe = 'error'
  assert existe == "done"
  print("Everything passed")'''

# COMMAND ----------

# MAGIC %md # Test Dataprep

# COMMAND ----------

test_dataprep_list = meta["test_dataprep"][0].split(",")
if __name__ == "__main__":
  for func in test_dataprep_list:
    log.start(F"[Test Dataprep] {func}")
    eval(func)
    log.stop(F"[Test Dataprep] {func}")
 
log.info("Test Dataprep: Passed")

# COMMAND ----------

# MAGIC %md # Test Load Model

# COMMAND ----------

test_load_model_list = meta["test_load_model"][0].split(",")
if __name__ == "__main__":
  for func in test_load_model_list: 
    log.start(F"[Test Load Model] {func}")
    eval(func)
    log.stop(F"[Test Load Model] {func}")
    
log.info("Test Load model: Passed")

# COMMAND ----------

# MAGIC %md # Test Metrics

# COMMAND ----------

log.start("[Test Metrics]")

if __name__ == "__main__":
  if float(meta["score_training_acc"][0]) <= 0.75:
    log.assert_failed("score_training_acc is grater than 0.75")
  if float(meta["AUC"][0]) <= 0.75:
    log.assert_failed("AUC is grater than 0.75")

log.stop("[Test Metrics]")

# COMMAND ----------

log.printall()