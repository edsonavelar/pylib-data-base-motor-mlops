#!/usr/bin/python3
import json
import requests
import os
import sys
import getopt
import time
import logging
import time


# Essa linha abaixo leva em consideração que o METADADO está presente no  
from METADATA import METADATA as meta 


# Criando o Logging
logging.basicConfig(filename='multitaskjob.log', level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger('multitaskjogcreate')

# Criando o console handler e setando leval para debug
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)

# Criando formatador do log
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

# Adicionando o fomatador ao console handler
ch.setFormatter(formatter)

# Adicionando console handler ao log
logger.addHandler(ch)

# Essa variável guarda a estrutura da dag
# Tu --> prod
__dag_config = """
{
  "name": "",
  "max_concurrent_runs": 1,
  "email_notifications": {
    "on_start": [
   
    ],
    "on_success": [
    
    ],
    "on_failure": [
      
    ]
    },

  "tasks": [
    {
      "task_key": "unit_test",
      "description": "Clean and prepare the data",
      "notebook_task": {
        "notebook_path": "TU"
      },
      "existing_cluster_id": "1228-220746-bqqkddxs",
      "timeout_seconds": 3600,
      "max_retries": 1,
      "retry_on_timeout": true
      
    },
    {
      "task_key": "to_prod",
      "description": "Perform an analysis of the data",
      "notebook_task": {
        "notebook_path": "PRO"
      },
      "depends_on": [
        {
          "task_key": "unit_test"
        }
      ],
      "existing_cluster_id": "1228-220746-bqqkddxs",
      "timeout_seconds": 3600,
      "max_retries": 1,
      "retry_on_timeout": true
    }
  ]
}

"""

def main():
    """ Este script é responsável por criar o multitask job dentro do databricks 
        
        usage: 'multitaskjob.py -s <db_host_url> -t <token>  -c <clusterid> -j <jobfile> -w <workpath> -n <jobname>)'

        Atributos
        ------------
        db_host_url: str
            URL do workspace no databricks. Ex: "https://adb-6840195589605290.10.azuredatabricks.net"
        token: str
            Token do databricks. Ex: bqqkddxsdfsfsdfsdfsfsdf
        clusterid: str
            Identificador do cluster. Ex: "1228-220746-bqqkddxs"
        jobfile: str
            Caminho para arquivo que descreve jobfile (.json)
        workpath: str
            Caminho no databricks para onde os artefatos irão Ex. /Shared
        jobname: str 
            Nome customizado do 

    """

    jobname=""
    on_failure = ""
    on_success = ""
    workpath =""
    clusterid=""
    db_host_url=""
    token=""

    usage = 'multitaskjob.py -s <db_host_url> -t <token>  -c <clusterid> -w <workpath> -n <jobname>)'
    argv = sys.argv[1:]

    try:
        opts, args = getopt.getopt(argv, 's:t:c:w:n', ['db_host_url=', 'token=','clusterid=','workpath=','jobname='])
    except getopt.GetoptError:
        print(usage)
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print(usage)
            sys.exit()
        elif opt in ('-s', '--db_host_url'):
            db_host_url = arg
        elif opt in ('-t', '--token'):
            token = arg
        elif opt in ('-c', '--clusterid'):
            clusterid = arg
        elif opt in ('-w', '--workpath'):
            workpath = arg
        elif opt in ('-n', '--jobname'):
            jobname = arg
      
    start_time = time.time()

    logger.debug('*'*100)
    logger.debug('*'*45 + 'DAG BEGIN '+'*'*45)
    logger.debug('*'*100)


    # Preenchendo informações na configuração da dag
    #  + jobname:               Nome do job. Informação obtida pela linha de comando
    #  + on_sucess:             Email se esteira for sucesso. Informação obtida do Metadado
    #  + on_failure:            Email se esteira for sucesso. Informação obtida do Metadado
    #  + existing_cluster_id:   Id do cluster 

    dag_config = json.loads(__dag_config)
    dag_config['name'] = jobname

    if 'on_success' in meta:
        for email in meta['on_success'][0].split(','):
            dag_config['email_notifications']['on_success'].append(email)
            logger.debug(F'email_notifications on sucess: {email}')


    if 'on_failure' in meta:
        for email in meta['on_failure'][0].split(','):
            dag_config['email_notifications']['on_failure'].append(email)
            logger.debug(F'email_notifications on on_failure: {email}')


    
    for i in dag_config['tasks']:
        i['notebook_task']['notebook_path'] = workpath + i['notebook_task']['notebook_path']
        i['existing_cluster_id'] = clusterid

    logger.debug("[METADATA] "+ str(dag_config) )
    logger.debug("Creating the Job")

    # Usa API do Databricks para criar o job ( o  job só é executado depois)
    resp = requests.post(db_host_url + '/api/2.1/jobs/create', data=json.dumps(dag_config), auth=("token", token))
    runjson = resp.text
    
    d = json.loads(runjson)   
    logger.debug(F"[job id] {d}") 
    # Pega o id do job para saber qual job será executado
    jobid = str( d['job_id'] )

    logger.debug(F"JOB Created with id: {jobid}")

    # Usa API do Databricks para executar o job criado anteriormente
    resp = requests.post(db_host_url + '/api/2.1/jobs/run-now', data="{\"job_id\": "+jobid+"}", auth=("token", token))

    runjson = resp.text
    d = json.loads(runjson)
    runid = str( d['run_id'] )
    logger.debug(F"JOB is running with id: {runid}")
    

    i=0
    waiting = True
    error = False
    error_message = ""
    TIMEOUT = 600

    while waiting:
        """
            Nesse momento o job está executando. 
            Usa-se a API via GET para pegar as informações do RUN usando o runid. 
            A cada 10 segundo, pega uma nova informação. O limite máximo de espera é 600s 
            O loop é quebrado caso o status do RUN for TERMINATED, INTERNAL_ERROR or SKIPPED 
        """
        time.sleep(10)
        
        url_request = db_host_url + '/api/2.1/jobs/runs/get?run_id='+str(runid)

        logger.debug("Get Job Info")
        logger.debug("url_request: "+url_request)

        jobresp = requests.get(url_request,data=json.dumps(dag_config), auth=("token", token))
        jobjson = jobresp.text

        logger.debug(jobjson)

        j = json.loads(jobjson)
        current_state = j['state']['life_cycle_state']
        runid = j['run_id']
        if current_state in ['TERMINATED'] or i >= TIMEOUT:
            break

        if current_state in ['INTERNAL_ERROR', 'SKIPPED'] or i >= TIMEOUT:
            state_message = j['state']['state_message']
            error_message = F"[Multi Task Job] Finished with Error | Status = {current_state} | Desc = {state_message}"

            logger.error(error_message)
            error = True
            
            break
        i=i+1

    
    seg = time.time() - start_time
    min = seg/60
    logger.debug(F"--- Execution Time ---")
    logger.debug(F"--- {seg} seconds ---")
    logger.debug(F"--- {min} minutes ---")

    if not error:
        logger.debug(F"Execution Status: [SUCCESS]")
        logger.debug('*'*45 + ' DAG  ENG '+'*'*45)
    else:
        logger.debug(F"Execution Status: [FAILURE]")
        logger.debug('*'*45 + ' DAG  ENG '+'*'*45)
        raise Exception(error_message)

if __name__ == '__main__':
  main()

# Examplo de Execução
# python multitaskjob.py --db_host_url="https://2286129091435213.3.gcp.databricks.com" --token='dapicebb8abfb77294ce53fbdccf2ea81f7f' --clusterid= '0317-174816-1651o90e' --workpath=/Shared/ --jobname="asdf"
