# README #

Essa bilbioteca contém os códigos que compoem o motor da nova esteira mlops

### Hierarquia ###

* Motor da esteira ( logs, testes unitarios e de produção)
    São códigos do motor da esteira de mlops
    * dags/log.py
    * dags/prod.py
    * dags/tu.py


* Utilitarios - Binary Handler e Mutitask
    São códigos auxiliares para criação de jobs e manipulação de arquivos.
    *  utils/binaryhandler.py
    * utils/multitaskjob.py

* [Documentação](#)

Para executar os scripts utilitários, basta installar com o pip e executar com parametro '-m'

Ex: o Comando abaixo executa o multitaskjob   

`$python -m mlops_motor.utils.multitaskjob`

Para os códios da dag, será necessário fazer unzip no código. É possível fazer isso também com os códigos do utils

