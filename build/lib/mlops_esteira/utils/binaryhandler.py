#!/usr/bin/python3
"""
Este script é responsável por manipular os binários dos modelos, 
ele utilizado o campo path_to_model do metadado para pegar o caminho do modelo
e salvar dentro do dbfs da pasta da engenharia.

Esse script é chamado de dentro do stage do Jenkins.
Mas pode ser executado via terminal caso seja necessário. 
"""
from METADATA import METADATA as meta 
import getopt
import sys
import subprocess
import os

def main():

    directory=""
    buildnumber="0"
    engpath=""

    try:
        opts, args = getopt.getopt(sys.argv[1:], 'r:b',
                                    ['reponame=', 'buildnumber=','dir=','engpath='])
    except getopt.GetoptError:
        print('binaryhandler.py -r <reponame> -b <buildnumber> -d <dir> -p <engpath>')
        sys.exit(2)

    for opt, arg in opts:
        if opt == '-h':
            print(
                'binaryhandler.py -r <reponame> -b <buildnumber> -d <dir> -p <engpath>')
            sys.exit()
        elif opt in ('-r', '--reponame'):
            reponame = arg
        elif opt in ('-b', '--buildnumber'):
            buildnumber = arg
        elif opt in ('-d', '--dir'):
            directory = arg
        elif opt in ('-p', '--engpath'):
            engpath = arg

    if 'path_to_model' in meta:

        if directory == "true":

            path_to_binary = F"{engpath}/{reponame}/{buildnumber}/"

            # Create Eng Folder
            query = F"databricks fs mkdirs dbfs:{path_to_binary}"
            print("[BINARY HANDLER: Create eng Folder] run: "+ query)
            os.system(query)

            if type(meta['path_to_model']) == list:
                path_to_model = meta['path_to_model'][0]
            else:
                path_to_model = meta['path_to_model']

            query = F"databricks fs cp -r --overwrite dbfs:{path_to_model} dbfs:{path_to_binary}"
            print("[BINARY HANDLER: Coping Folder] run: "+ query)
            os.system(query)

            with open('METADATA.py', 'w') as f:
                meta['path_to_binary'] = path_to_binary
                f.write('METADATA='+ str(meta) )

            print(query)
            pass

        else:
            query = 'databricks fs mkdirs dbfs:/FileStore/Eng/Models/' + reponame + "/" + buildnumber
            print("[BINARY HANDLER2] run: "+ query)
            os.system(query)

            path_to_binary = F"{engpath}/{reponame}/{buildnumber}/binary.zip"

            if type(meta['path_to_model']) == list:
                path_to_model = meta['path_to_model'][0]
            else:
                path_to_model = meta['path_to_model']

            query = "databricks fs cp dbfs:"+ path_to_model +"/binary.zip dbfs:" +  path_to_binary
            print("[BINARY HANDLER3] run: "+ query)
            os.system(query)
            
            with open('METADATA.py', 'w') as f:
                meta['path_to_binary'] = path_to_binary
                f.write('METADATA='+ str(meta) )
    else:
        raise Exception("path_to_model doesn't exists in metadata")


if __name__ == '__main__':
  main()