# Databricks notebook source
import logging
import datetime
from time import time
from datetime import timedelta
import subprocess

def _create_assert_failed_loglevel():
    "Add TRACE log level and Logger.trace() method."

    logging.ASSERT_FAILED = 15
    logging.addLevelName(logging.ASSERT_FAILED, "ASSERT_FAILED")

    def _assert_failed(logger, message, *args, **kwargs):
        if logger.isEnabledFor(logging.ASSERT_FAILED):
            logger._log(logging.ASSERT_FAILED, message, args, **kwargs)

    logging.Logger.assert_failed = _assert_failed

_create_assert_failed_loglevel()

class LogDB():
  """ This class extends a logging functionality """
  def __init__(self, nome="mlops.log", p_logfile ='/tmp/modelo001.log'):
    file_date = datetime.datetime.fromtimestamp(time()).strftime('%Y-%m-%d-%H-%M-%S')
    
    p_filename = 'custom_log'+file_date+'.log'
    self.p_logfile = p_logfile
    print(self.p_logfile)

    logger = logging.getLogger(nome)
    logger.setLevel(logging.DEBUG)

    fh = logging.FileHandler(self.p_logfile,mode='a')
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)
    ch.setFormatter(formatter)

    if(logger.hasHandlers()):
      logger.handlers.clear()

    logger.addHandler(fh)
    logger.addHandler(ch)
    
    self.debug   = logger.debug
    self.info    = logger.info
    self.warn = logger.warning
    self.critical= logger.critical
    self.exception = logger.exception
    self.logger = logger
    
    self.timer_hist = {}
  
  def get_filename(self):
    return self.p_logfile
  
  def mvto(self,destino):
    dbutils.fs.mv("file:" + self.p_logfile, destino) 
    
  def start(self,name: str):
    timer_begin = time()
    self.timer_hist[name] = timer_begin
    timer_begin_str = datetime.datetime.fromtimestamp(timer_begin).strftime('%Y-%m-%d %H:%M:%S')
    self.debug(F"Timer(start): {name} : {timer_begin_str}")
    
  def strfdelta(self,tdelta, fmt):
    print("days: ",tdelta.days)
    d = {"days": tdelta.days}
    d["hours"], rem = divmod(tdelta.seconds, 3600)
    d["minutes"], d["seconds"] = divmod(rem, 60)
    return fmt.format(**d)
  
  def stop(self,name: str,comm = ""):
    
    if name not in self.timer_hist.keys():
      raise Exception(F"There is no timer with name: {name}")
      
    timer_stop = time()
    elapsed = timer_stop - self.timer_hist[name]
    elaped_delta = (timedelta(seconds=elapsed))#.strftime('%Y-%m-%d-%H-%M-%S')
    
    timer_stop_str = datetime.datetime.fromtimestamp(timer_stop).strftime('%Y-%m-%d %H:%M:%S')
    self.debug(F"Timer(stop): {name} : {timer_stop_str}")
    
    elapsed_str = self.strfdelta(elaped_delta, "{hours}h:{minutes}m:{seconds}s")
    self.debug(F"{name} SUCCESS After: {elapsed_str}")
    
    self.timer_hist.pop(name)
    return elapsed_str #str(timedelta(seconds=elapsed)).strftime('%Y-%m-%d-%H-%M-%S')
  
  def error(self,msg):
    self.logger.error(msg)
    raise Exception(msg)
    
  def assert_failed(self, message, *args, **kwargs):
    self.logger._log(logging.ASSERT_FAILED, message, args, **kwargs)
    raise Exception(message)
    
  def shutdown(self):
    self.timer_hist.clear()
    logger.shutdown()
    
  def printall(self):
    result = subprocess.run(['cat',self.p_logfile], stdout=subprocess.PIPE)
    print(result.stdout.decode("utf-8") )
