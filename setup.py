from setuptools import find_packages, setup

setup(
    name='mlops_motor',
    author="edson.avelar@bv.com.br, daniel.vargas@bv.com.br",
    packages=find_packages(include=['mlops_motor','mlops_motor.dags','mlops_motor.utils']),
    version='0.1.0',
    description='Scripts do Motor da Esteira MLOPS',
    install_requires=[],
    license='Banco Votorantim',
    url="URL para o Confluence",
    classifiers=[
        'Programming Language :: Python :: 3',
        'Topic :: Utilities',
        'License :: OSI Approved :: Banco Votorantim License'
    ]
)